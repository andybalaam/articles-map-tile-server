# Self-hosting a map tile server for use with Element location sharing

Element and several other Matrix clients have the ability to share your location with other people.

When you share your location, it will show up on a map which you can zoom and move around on to look at the surrounding area.

To display these maps, clients need to communicate with a map tile server, which provides images of the map information at each co-ordinate and zoom level.

Matrix.org provides a tile server at TODO, that clients can use to display maps, and provides a comprehensive [privacy policy](TODO) so you can be reassured that your data is protected.

But, even if you want to make sure no location data leaves your network, you can still use location sharing in Element, by running your own map tile server!

## Overview

## Downloading and transforming the data

## Serving the tiles

## Configuring Element
